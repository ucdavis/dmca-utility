package ucd.lookup.run;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EmailDetail {

	public static void getEmailDetail (String mothraid, Person person) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("file:///c:\\apps\\configuration\\applicationcontext-datasource.xml");
		//ApplicationContext context = new ClassPathXmlApplicationContext("file:///${UCDLOOKUPHOME}/config/applicationcontext-datasource.xml");
        DataSource dataSource = (DataSource) context.getBean("dataSource1");
        Connection con3 = null;
		try {
			con3 = dataSource.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println ( "Error getconnection");	
		}
	
		try {
		
			/*  
			 *  get email detail
			 */
			String query = null;
			query = 
					"select mailid, hostpart "+
				    "from mothra.mailids "+
				    "where mothraid = '" + mothraid + "'";

			PreparedStatement ps = con3.prepareStatement(query);
			ResultSet rs = ps.executeQuery();

			while ( rs.next() ) {

				String hostpart = null;
				hostpart = rs.getString (2);
				int pos = 0;
				int nsize = 0;
				nsize = hostpart.length();
			    pos = hostpart.indexOf("gmx.");

			    if (pos != -1) {
			    	hostpart = hostpart.substring (pos+4, nsize);
			    }
				
				person.setEmail (rs.getString (1)+'@'+ hostpart);
			}
		
			try {
				rs.close();
				con3.close();
			} catch (Exception e) {System.out.println("error closing connections");}
		

		} catch (SQLException e) {
			System.out.println(" error "+e.getMessage() );
		}	
	
	}
	
}
