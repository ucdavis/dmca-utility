package ucd.lookup.run;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProcessSubnet {

	/*  
	 * get VLAN information 
     */
	public static void getSubnet (String s, Subnet subnet) throws JSONException {
			
		final JSONObject obj1 = new JSONObject(s);
		final JSONArray data1 = obj1.getJSONArray("subnet");
		final int n1 = data1.length();
		for (int i = 0; i < n1; ++i) {
			final JSONObject gvlan = data1.getJSONObject(i);
		    subnet.setFirst(gvlan.getString("first"));  
		    subnet.setLast(gvlan.getString("last"));  
		    subnet.setGateway(gvlan.getString("gateway"));  
		    subnet.setSubnet_mask(gvlan.getString("subnet_mask"));  
		}
			
	}
}
