package ucd.lookup.run;

public class Person {
	
	private String mothraid;
	private String lastname;
	private String firstname;
	private String middlename;
	private String loginid;	
	private String email;	
	private String clientytpe;	
	private String department;	
	private String major;	
	private String level;	
	private String status;
	
	@Override
	public String toString() {
		return "Person [mothraid=" + mothraid + ", lastname=" + lastname + ", firstname=" + firstname + ", middlename="
				+ middlename + ", loginid=" + loginid + ", email=" + email + ", clientytpe=" + clientytpe
				+ ", department=" + department + ", major=" + major + ", level=" + level + ", status=" + status + "]";
	}
	
	public Person(String mothraid, String lastname, String firstname, String middlename, String loginid, String email,
			String clientytpe, String department, String major, String level, String status) {
		super();
		this.mothraid = mothraid;
		this.lastname = lastname;
		this.firstname = firstname;
		this.middlename = middlename;
		this.loginid = loginid;
		this.email = email;
		this.clientytpe = clientytpe;
		this.department = department;
		this.major = major;
		this.level = level;
		this.status = status;
	}	

    public Person () {}
    
	public String getMothraid() {
		return mothraid;
	}
	public void setMothraid(String mothraid) {
		this.mothraid = mothraid;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getClientytpe() {
		return clientytpe;
	}
	public void setClientytpe(String clientytpe) {
		this.clientytpe = clientytpe;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

	
}
