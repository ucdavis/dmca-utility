package ucd.lookup.run;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PersonService {

	/*  
	 * get mothra id
     */
	public static void getMothraId (String login, Person person) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("file:///c:\\apps\\configuration\\applicationcontext-datasource.xml");
		//ApplicationContext context = new ClassPathXmlApplicationContext("file:///${UCDLOOKUPHOME}/config/applicationcontext-datasource.xml");
        DataSource dataSource = (DataSource) context.getBean("dataSource1");
        Connection con2 = null;
		try {
			con2 = dataSource.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println ( "Error getconnection");	
		}
		
		try {
			
			/*  	
		     *  get login part
		    */
			String query = null;
		    String login1 = null;
		    int pos = 0;
		    pos = login.indexOf("@");
		    login1 = login;
		    if (pos != -1) {
		    	login1 = login.substring (0, pos);
		    }
		    
			/*  
		     *  get Mothra Id
		    */
			query = "select mothraid "+ 
		            "from mothra.accounts "+
		            "where loginid = '"+ login1 +"'";

			PreparedStatement ps = con2.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			
			while ( rs.next() ) {
				System.out.println("Person: "+rs.getString (1) );
				person.setMothraid(rs.getString (1));
			}
			
			try {
			rs.close();
			con2.close();
			} catch (Exception e) {System.out.println("error closing connections");}
			

		} catch (SQLException e) {
			System.out.println(" error "+e.getMessage() );
		}	
	}	
		
}
