package ucd.lookup.run;

public class Subnet {

	private String first;
	private String last;
	private String gateway;
	private String subnet_mask;
	
	public Subnet() {}
	
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		this.first = first;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		this.last = last;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public String getSubnet_mask() {
		return subnet_mask;
	}
	public void setSubnet_mask(String subnet_mask) {
		this.subnet_mask = subnet_mask;
	}
	@Override
	public String toString() {
		return "Subnet [first=" + first + ", last=" + last + ", gateway=" + gateway + ", subnet_mask=" + subnet_mask
				+ "]";
	}
	
	public Subnet(String first, String last, String gateway, String subnet_mask) {
		super();
		this.first = first;
		this.last = last;
		this.gateway = gateway;
		this.subnet_mask = subnet_mask;
	}
	
	
}
