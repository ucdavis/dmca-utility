package ucd.lookup.run;

public class Transaction {

	// ip, mac, timestamp, first_seen, loginid	
	private String ip;
	private String mac;
	private String timestamp;
	
	@Override
	public String toString() {
		return "Transaction [ip=" + ip + ", mac=" + mac + ", timestamp=" + timestamp + ", first_seen=" + first_seen
				+ ", longid=" + longid + "]";
	}
	private String first_seen;
	private String longid;
	public String getIp() {
		return ip;
	}
	
	
	public Transaction(String ip, String mac, String timestamp, String first_seen, String longid) {
		super();
		this.ip = ip;
		this.mac = mac;
		this.timestamp = timestamp;
		this.first_seen = first_seen;
		this.longid = longid;
	}

    public Transaction () {}
    
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getFirst_seen() {
		return first_seen;
	}
	public void setFirst_seen(String first_seen) {
		this.first_seen = first_seen;
	}
	public String getLongid() {
		return longid;
	}
	public void setLongid(String longid) {
		this.longid = longid;
	}
	  
}
