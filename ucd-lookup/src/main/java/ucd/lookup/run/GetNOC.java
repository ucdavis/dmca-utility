package ucd.lookup.run;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class GetNOC {    // s = GetNOC.getNOCList ( ip );

	public static String getNOCList (String ip) {
			
		String s = null;
  		RestTemplate restTemplate = new RestTemplate();
        String uri = "https://cr-rest-dev.ucdavis.edu/getVlan/{ip}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("cr-rest-api-key", "5656257035");
        HttpEntity<String> entity = new HttpEntity<String>("Running", headers);
        
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("ip", ip);

		try {
			ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class, vars);
			//System.out.println( response.getBody() );
			s = response.getBody();
		} catch (Exception e) {
			System.out.println( e.getMessage()+" : "+e.getClass() );
		}
		    		
		return s;
					
	}
	
}
