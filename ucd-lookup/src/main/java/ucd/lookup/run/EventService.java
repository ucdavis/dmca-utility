package ucd.lookup.run;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class EventService {

	/*  
	 * get event information
     */
	public static String getEventInfo (String ip, String date, Transaction transaction) {
		

		String sEventService = "";
		
		ApplicationContext context = new ClassPathXmlApplicationContext("file:///c:\\apps\\configuration\\applicationcontext-datasource.xml");
		//ApplicationContext context = new ClassPathXmlApplicationContext("file:///${UCDLOOKUPHOME}/config/applicationcontext-datasource.xml");
		
		DataSource dataSource = (DataSource) context.getBean("dataSource");
        Connection con = null;
		try {
			con = dataSource.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println ( "Error getconnection");	
		}

		try {
			String query = null;
            query = 
        		"select * "+      // ip, mac, timestamp, first_seen, loginid
        		"from alerts.arp_archive  "+
        		"where (ip = '"+ ip +"') "+
        		"and (first_seen <= '"+ date +"') "+
        		"and (timestamp > '"+ date +"') "+
        		"order by first_seen desc";        		
        
			PreparedStatement ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			
			while ( rs.next() ) {
				sEventService = rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+
						rs.getString(4)+"    "+rs.getString(5);
				
				transaction.setIp(ip);
				transaction.setMac(rs.getString(2));
				transaction.setTimestamp(rs.getString(3));
				transaction.setFirst_seen(rs.getString(4));
				transaction.setLongid(rs.getString(5));					

			}
			
			try {
				rs.close();
				con.close();

			} catch (Exception e) {System.out.println("error closing connections");}
			
		} 	catch (SQLException e) {
			System.out.println ("error connecting "+e.getMessage());	
		}
		
		return sEventService;
		
	}

}

