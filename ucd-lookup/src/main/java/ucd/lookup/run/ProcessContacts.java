package ucd.lookup.run;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ProcessContacts {

	/*  
	 * get contacts information 
     */
	public static void getContacts (String s, ArrayList<Contact>contacts) throws JSONException {
        
		Contact contact = null;
        
		final JSONObject obj1 = new JSONObject(s);
		final JSONArray data1 = obj1.getJSONArray("contact");
		final int n1 = data1.length();
		for (int i = 0; i < n1; ++i) {
			final JSONObject gvlan = data1.getJSONObject(i);
		    contact = new Contact();
			contact.setName(gvlan.getString("full_name"));
			contact.setPhone(gvlan.getString("workphone"));
			contacts.add( contact );
		}
	}
	
}
