package ucd.lookup.run;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PersonDetail {

	public static void getPersonDetail (String mothraid, Person person) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("file:///c:\\apps\\configuration\\applicationcontext-datasource.xml");
		//ApplicationContext context = new ClassPathXmlApplicationContext("file:///${UCDLOOKUPHOME}/config/applicationcontext-datasource.xml");
        DataSource dataSource = (DataSource) context.getBean("dataSource1");
        Connection con3 = null;
		try {
			con3 = dataSource.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println ( "Error getconnection");	
		}
	
		try {
			/*  
			 *  get person detail
			 */
			String query = null;
			query = 
					"select p.mothraid, p.lastname, p.firstname, p.middlename, a.description as clienttype, "+ 
				    "b.description as status, c.major, d.classlevel, e.department, p.deptcode "+
				    "from mothra.people p "+
				    "left outer join mothra.codes a "+
				    "on (p.clienttype = a.code) "+
				    "and (a.tablename= 'PEOPLE') "+
				    "and (a.columnname = 'CLIENTTYPE') "+
				    "left outer join mothra.codes b "+
				    "on (p.clientstatus = b.code) "+
				    "and (b.tablename='PEOPLE') "+
				    "and (b.columnname = 'CLIENTSTATUS') "+
				    "left outer join mothra.majors c "+
				    "on (p.majorcode = c.majorcode) "+
				    "left outer join mothra.classlevels d "+
				    "on (p.levelcode = d.levelcode) "+
				    "left outer join mothra.departments e "+
				    "on (p.deptcode = e.deptcode) "+
				    "where mothraid = '" + mothraid +"' ";

			PreparedStatement ps = con3.prepareStatement(query);
			ResultSet rs = ps.executeQuery();

			while ( rs.next() ) {
				
				person.setLastname(rs.getString (2));
				person.setFirstname(rs.getString (3));
				person.setMiddlename(rs.getString (4));
				person.setClientytpe(rs.getString (5));
				person.setStatus(rs.getString (6));
				person.setMajor(rs.getString (7));
				person.setLevel(rs.getString (8));
				person.setDepartment(rs.getString (9));
				
			}
		
			try {
				rs.close();
				con3.close();
			} catch (Exception e) {System.out.println("error closing connections");}
		

		} catch (SQLException e) {
			System.out.println(" error "+e.getMessage() );
		}	
	
	}
	
}
