package ucd.lookup.run;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ProcessVlan {

	/*  
	 * get service number 
     */
	public static String getVlan (String s) throws JSONException {
		
		String sNumber = "";
		final JSONObject obj1 = new JSONObject(s);
		final JSONArray data1 = obj1.getJSONArray("vlan");
		final int n1 = data1.length();
		for (int i = 0; i < n1; ++i) {
			final JSONObject gvlan = data1.getJSONObject(i);
		    sNumber = gvlan.getString("service_number");
		}
		return sNumber;
		
	}
}
