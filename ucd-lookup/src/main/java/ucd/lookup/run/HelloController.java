package ucd.lookup.run;


import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import org.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class HelloController {
	

	
	@RequestMapping(value="/",method=RequestMethod.GET)
    public String greetingForm(Model model) {

	    //System.setProperty("javax.net.ssl.trustStore", "c:\\apps\\glassfish4\\glassfish\\domains\\domain1\\config\\keystore.jks");
	    //System.setProperty("javax.net.ssl.truststorePassword", "changeit");	    

		model.addAttribute("event", new Event());
        return "ipform";
    }	

	@RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
	public String accessDenied(Model model) {
	      model.addAttribute("event", new Event());
	        return "accessDenied";
	}

	@RequestMapping(value="/result",method=RequestMethod.POST)
	public String greetingSubmit(@ModelAttribute("SpringWeb") Event event, Model model) throws ParseException { 
	        
	    /*  
	     * construct correct date format 
	     */
	    String timestamp =  event.gettimestamp();	    
	    try {
			timestamp = ConstructDate.getTheDate( timestamp );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
        /*  
    	 * get security information 
    	 * Create transaction object
         */
        String sTransaction = "";
        Transaction transaction = new Transaction();
        try {
        	sTransaction = EventService.getEventInfo (event.getip(), timestamp, transaction);
        } catch (Exception e) {  sTransaction = "";}
        
        
        if (sTransaction.isEmpty()) {        
       
            String servicenumber = "";
            
            /*  
        	 * verify IP wireless 
        	 * 
             */
            int pos;
    	    pos = event.getip().indexOf("168.150.");
    	    if (pos != -1) { 
    	        System.out.println(" ip: "+event.getip());
    		  	servicenumber = "";
    	   
        	    model.addAttribute("ip", event.getip());
            	model.addAttribute("timestamp", timestamp);
            	return "nomatch";
 	    
    	    }
    	    else {
    	    
    	    	/*  
    	    	 * get NOC information from current IP 
    	    	 */
    	    	String s = null;
    	    	try {
    	    		s = GetNOC.getNOCList ( event.getip() );
    	    	} catch (Exception e) {}
    	    	
    	    	String sReplaced = s.replace('"', '\"');
    	    	try {
    	    		servicenumber = ProcessVlan.getVlan (sReplaced);
    	    	} catch (JSONException e) {}
                        
    	       	if (servicenumber !="") {
            		
            		Subnet subnet = new Subnet();
            		try {
            			ProcessSubnet.getSubnet (sReplaced, subnet);
        	    	} catch (JSONException e) {}
  
                    model.addAttribute("servicenumber", servicenumber);
            		model.addAttribute("first", subnet.getFirst());
            		model.addAttribute("last", subnet.getLast());
            		model.addAttribute("gateway", subnet.getGateway());
            		model.addAttribute("subnet_mask", subnet.getSubnet_mask());
            
                    ArrayList<Contact> list = new ArrayList<Contact>();
          			try {
          				ProcessContacts.getContacts (sReplaced, list );
        	    	} catch (JSONException e) {}
                    
            		model.addAttribute("list", list);
            		
           			return "departmental";    	       	
    	       	}
           		else {
                    model.addAttribute("ip", event.getip());
                    model.addAttribute("timestamp", timestamp);
                    return "nomatch";
                }
    	    }
    	    
        }
        else {

    		/*  
        	 * get MOTHRA id
             */
    	    Person person = new Person();
    		PersonService.getMothraId (transaction.getLongid(), person);
        	//System.out.println("** mothraid: "+person.getMothraid());


        	/*  
        	 * get personal information
             */
    		PersonDetail.getPersonDetail (person.getMothraid(), person);


    		/*  
        	 * get email information
             */
    		EmailDetail.getEmailDetail (person.getMothraid(), person);
    	        	
        	model.addAttribute("ip", event.getip());
        	model.addAttribute("first", transaction.getFirst_seen());
        	model.addAttribute("second", transaction.getTimestamp());
        	model.addAttribute("loginid", transaction.getLongid());
		 	model.addAttribute("name", person.getFirstname()+" "+person.getLastname());
    		model.addAttribute("loginid", transaction.getLongid());
    		model.addAttribute("email", person.getEmail());
    		model.addAttribute("clienttype", person.getClientytpe());
    		model.addAttribute("department", person.getDepartment());
    		model.addAttribute("major", person.getMajor());
    		model.addAttribute("level", person.getLevel());
    		model.addAttribute("status", person.getStatus());

    		model.addAttribute("timestamp", timestamp);
    		
        	return "resultform";
		
        }
	
	}
		
}

