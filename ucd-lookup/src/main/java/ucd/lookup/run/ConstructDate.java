package ucd.lookup.run;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ConstructDate {

		/*  
		 * process date as type YYYY-MM-DDTHH:MM:SSZ
		 * return date as       YYYY-MM-DD HH:MM:SS
	     */
		public static String getTheDate (String s) throws ParseException {
		
			String correctedDate = null;
			DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

			int pos;
	        pos = s.indexOf("T");
	    	correctedDate = "1999-01-01 01:01:00";
	    	
	    	if (pos > 0) {
	    		try {
	    			Date date = utcFormat.parse(s+"Z");  
		
	    			DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss.SS");
	    			pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));

	    			//System.out.println(pstFormat.format(date));
	    			correctedDate = pstFormat.format(date);
	    			correctedDate = correctedDate.replace ("T", " ");
	    			correctedDate = correctedDate.replace ("Z","");
	    		} catch (ParseException ex) {
	        		ex.printStackTrace();
	        		correctedDate = "1999-01-01 01:01:00";
	    		}
	        
	    	}
	    	
	    	return correctedDate;
				
		}
			
}
