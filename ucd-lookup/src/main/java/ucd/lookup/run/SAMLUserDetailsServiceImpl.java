package ucd.lookup.run;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.schema.impl.XSAnyImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;

/**
 * Implementation of SAMLUserDetailsService that retrieves attributes for the authenticated user
 * and adds any needed roles.
 *
 * Further qualification of the authenticate user can also be done here with additional roles added
 * where needed.
 */
public class SAMLUserDetailsServiceImpl implements SAMLUserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(SAMLUserDetailsServiceImpl.class);
    private static final Marker SAMLMarker = MarkerFactory.getMarker("SAML_USER_DETAILS");

    public static final String DUMMY_PASSWORD = "DUMMY_PASSWORD";

    private List<String> adminUsers;

    public void setAdminUsers(List<String> adminUsers) {
        this.adminUsers = adminUsers;
    }

    @Override
    public Object loadUserBySAML(SAMLCredential credential) throws UsernameNotFoundException {

        // Dump all SAML attributes when debug is enabled
        if (logger.isDebugEnabled())
        {
            this.dumpSAMLAttributes(credential);
        }

        // Get the principal name of the user from the assertion
        String username = this.getPrincipalName(credential.getAuthenticationAssertion());

        // Return the username and authorities.
        return new User((username == null ?
                credential.getNameID().getValue() : username), DUMMY_PASSWORD,  getAuthorities(username));
    }

    /**
     * Get the GrantedAuthorities for the current user
     * @return List of GrantedAuthorities
     */
    private Collection<? extends GrantedAuthority> getAuthorities(String username) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();

        if (username != null)
        {
            // Default role for all authenticated users
            authList.add(new SimpleGrantedAuthority("ROLE_USER"));

            // Check that this particular user qualifies as an admin
            if (this.adminUsers != null && this.adminUsers.contains(username))
            {
                authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            }
        }

        return authList;
    }

    /**
     * Get the principal name of the authenticated user from the SAML assertion
     *
     * @param assertion
     * @return
     */
    private String getPrincipalName(Assertion assertion)
    {
        for (AttributeStatement attributeStatement : assertion.getAttributeStatements())
        {
            for (Attribute attribute : attributeStatement.getAttributes())
            {
                if (attribute.getName().endsWith("oid:"+SAMLAttribute.EDUPERSON_PRINCIPAL_NAME.getOid()))
                {
                    List<XMLObject> attributeValues = attribute.getAttributeValues();
                    if (!attributeValues.isEmpty())
                    {
                        // Return the username stripped of domain
                        return getAttributeValue(attributeValues.get(0)).replaceAll("(@.*)$", " ").trim();
                    }
                }
            }
        }
        logger.info(SAMLMarker, "No "+SAMLAttribute.EDUPERSON_PRINCIPAL_NAME.getName()+" found for this user!");
        return null; //throw new IllegalArgumentException("no username attribute found");
    }

    /**
     * Get the string value for the input SAML XMLObject object
     *
     * @param XMLObject attributeValue
     * @return
     */
    private String getAttributeValue(XMLObject attributeValue)
    {
        return attributeValue == null ?
                null :
                    attributeValue instanceof XSString ?
                            getStringAttributeValue((XSString) attributeValue) :
                                attributeValue instanceof XSAnyImpl ?
                                        getAnyAttributeValue((XSAnyImpl) attributeValue) :
                                            attributeValue.toString();
    }

    /**
     * Get the string value for the input SAML XXString object
     *
     * @param XSString attributeValue
     * @return String
     */
    private String getStringAttributeValue(XSString attributeValue)
    {
        return attributeValue.getValue();
    }

    /**
     * Get the string value for the input SAML XSAnyImpl object
     *
     * @param XSAnyImpl attributeValue
     * @return String
     */
    private String getAnyAttributeValue(XSAnyImpl attributeValue)
    {
        return attributeValue.getTextContent();
    }

    /**
     * Dump the SAML attributes
     * @param credential
     */
    private void dumpSAMLAttributes(SAMLCredential credential)
    {
        Assertion assertion = credential.getAuthenticationAssertion();
        logger.debug(SAMLMarker, "Authenticated SAML NameID: "+credential.getNameID().getValue());
        for (AttributeStatement attributeStatement : assertion.getAttributeStatements())
        {
            for(Attribute attribute : attributeStatement.getAttributes())
            {
                logger.debug(SAMLMarker, attribute.getFriendlyName()+"("+attribute.getName()+"): "+this.getAttributeValue(attribute.getAttributeValues().get(0)));
            }
        }
    }

   /**
    * Enumeration of eduPerson attributes for the purpose of defining the OID
    *
    * The eduPerson Object Class Spec is located at the link belowL:
    * http://software.internet2.edu/eduperson/internet2-mace-dir-eduperson-201310.html
    */
    public enum SAMLAttribute
    {
        EDUPERSON_AFFILLIATION("eduPersonAffiliation","1.3.6.1.4.1.5923.1.1.1.1"),
        EDUPERSON_NICKNAME("eduPersonNickname","1.3.6.1.4.1.5923.1.1.1.2"),
        EDUPERSON_ORG_DN("eduPersonOrgDN","1.3.6.1.4.1.5923.1.1.1.3"),
        EDUPERSON_ORG_UNIT("eduPersonOrgUnitDN","1.3.6.1.4.1.5923.1.1.1.4"),
        EDUPERSON_PRIMARY_AFFILLIATION("eduPersonPrimaryAffiliation","1.3.6.1.4.1.5923.1.1.1.5"),
        EDUPERSON_PRINCIPAL_NAME("eduPersonPrincipalName","1.3.6.1.4.1.5923.1.1.1.6"),
        EDUPERSON_ENTITLEMENT("eduPersonEntitlement","1.3.6.1.4.1.5923.1.1.1.7"),
        EDUPERSON_ORG_UNIT_DN("eduPersonPrimaryOrgUnitDN","1.3.6.1.4.1.5923.1.1.1.8"),
        EDUPERSON_SCOPED_AFFILLIATION("eduPersonScopedAffiliation","1.3.6.1.4.1.5923.1.1.1.9"),
        EDUPERSON_TARGET_ID("eduPersonTargetedID","1.3.6.1.4.1.5923.1.1.1.10"),
        EDUPERSON_ASSURANCE("eduPersonAssurance","1.3.6.1.4.1.5923.1.1.1.11"),
        EDUPERSON_PRINCIPAL_NAME_PRIOR("eduPersonPrincipalNamePrior","1.3.6.1.4.1.5923.1.1.1.12"),
        EDUPERSON_UNIQUE_ID("eduPersonUniqueId","1.3.6.1.4.1.5923.1.1.1.13"),
        UID("uid","0.9.2342.19200300.100.1.1"),
        CN("cn","2.5.4.3"),
        OU("ou","2.5.4.11"),
        O("o","2.5.4.10"),
        SN("sn","2.5.4.4"),
        TELEPHONE_NUMBER("telephoneNumber","2.5.4.20"),
        MAIL("mail","0.9.2342.19200300.100.1.3"),
        GIVENNAME("givenName","2.5.4.42");

        private String name;
        private String oid;

        private SAMLAttribute(String name, String oid)
        {
            this.name = name;
            this.oid = oid;

        }

        public String getName()
        {
            return this.name;
        }

        public String getOid()
        {
            return this.oid;
        }

    }
}
/*
    public static final String DUMMY_PASSWORD = "DUMMY_PASSWORD";
    private List<String> roles;

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
    @Override
    public Object loadUserBySAML(SAMLCredential credential) throws UsernameNotFoundException {
        String username = credential.getNameID().getValue();

        for (Attribute attribute : credential.getAttributes())
        {
            // Get the eduPersonPrinciplaName which is in the form of <principalName>@<domain>
            if (attribute.getFriendlyName().equals("eduPersonPrincipalName"))
            {
                XSString xsstring = (XSString)attribute.getAttributeValues().get(0);
                // Remove the @<domain> leaving only the principalName
                username = xsstring.getValue().replaceAll("(@.*)$", " ").trim();
            }
        }

        // Default roles from the XML config are loaded here.
        Collection<GrantedAuthority> gas = new ArrayList<GrantedAuthority>();
        for (String role : roles) {
            gas.add(new SimpleGrantedAuthority(role));
        }

        // Any additional role(s) based specifically on the authenticated user could be added here as well.

        return new User(username, DUMMY_PASSWORD, gas);
    }
}
*/